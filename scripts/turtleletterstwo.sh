#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[7.5, 0.0, 0.0]' '[0.0, 0.0, 7.5]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 0.25]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
