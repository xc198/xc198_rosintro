'''
This program uses MoveIt! to move the UR5 robot arm to draw the letter C.
'''

from __future__ import print_function
import sys
import copy
import time
from math import pi, tau, cos, sin

import rospy
import moveit_commander
from moveit_commander.conversions import pose_to_list
from geometry_msgs.msg import Pose

# Define tau as 2 times pi for convenience
tau = 2.0 * pi


def initialize():
    '''
    Initializes the MoveIt! commander and ROS node.

    Returns:
        move_group (MoveGroupCommander object): The primary interface to the manipulator group of joints.
    '''
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("C_motion_planning", anonymous=True)

    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    print("\n[INFO] Current robot pose:")
    print(move_group.get_current_pose().pose)
    return move_group


def set_pose_goal(move_group, x, y, z, orientation):
    '''
    Sets the pose goal for the robot arm to a specific position and orientation.

    Parameters:
        move_group (MoveGroupCommander object): The interface to the manipulator group of joints.
        x (float): X-coordinate of the position.
        y (float): Y-coordinate of the position.
        z (float): Z-coordinate of the position.
        orientation (tuple): Quaternion orientation (x, y, z, w).
    '''
    pose_goal = Pose()
    pose_goal.position.x = x
    pose_goal.position.y = y
    pose_goal.position.z = z
    pose_goal.orientation.x = orientation[0]
    pose_goal.orientation.y = orientation[1]
    pose_goal.orientation.z = orientation[2]
    pose_goal.orientation.w = orientation[3]

    move_group.set_pose_target(pose_goal)
    move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()
    print("[INFO] Robot moved to pose:", pose_goal)


def draw(move_group):
    '''
    Plans and executes a Cartesian path to draw the letter 'C'.

    Parameters:
        move_group (MoveGroupCommander object): The interface to the manipulator group of joints.
    '''
    print("\n[INFO] Preparing to draw the letter 'C'...")
    waypoints = []

    # Get the current pose to use as a starting point
    wpose = move_group.get_current_pose().pose

    # Generate waypoints for a semi-circular path to draw 'C'
    arc_length = 20  # Number of waypoints in the arc
    print("\n[INFO] Generating waypoints for the letter 'C':")
    for i in range(arc_length):
        angle = tau * (i / float(arc_length))
        wpose.position.x += cos(angle) * 0.01  # X-coordinate
        wpose.position.y += sin(angle) * 0.01  # Y-coordinate
        print(
            f"  Waypoint {i + 1}: x = {wpose.position.x:.4f}, y = {wpose.position.y:.4f}")
        waypoints.append(copy.deepcopy(wpose))

    # Compute Cartesian path
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    print(
        f"\n[INFO] Path planning complete. Path coverage: {fraction * 100:.2f}% of waypoints.")

    # Execute the planned path
    if move_group.execute(plan, wait=True):
        print("[INFO] Drawing 'C' complete.")
    else:
        print("[ERROR] Execution failed.")
    move_group.clear_pose_targets()


def main():
    '''
    Main function to execute the program.
    '''
    move_group = initialize()

    # Set the starting pose for drawing 'C'
    set_pose_goal(move_group,
                  0.48661254754581196, 0.10902330703318201, 0.42685791864135236,
                  (-0.00012970925451396507, -0.9999800978910051, 3.615814851281461e-05, 0.006307589871993692))

    # Draw the letter 'C'
    draw(move_group)


# Run the main function
main()
