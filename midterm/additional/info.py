#!/usr/bin/env python
from __future__ import print_function
import sys
import copy
import time
from math import pi, tau, cos, sin

import rospy
import moveit_commander
from moveit_commander.conversions import pose_to_list
from geometry_msgs.msg import Pose

# Define tau as 2 times pi for convenience
tau = 2.0 * pi


def initialize():
    """
    Initializes the MoveIt! commander and ROS node.
    """
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("C_motion_planning", anonymous=True)

    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)
    print("\n[INFO] Robot initialized.")
    return move_group


def move_to_start(move_group):
    """
    Moves the robot to the starting position for drawing the letter C.
    """
    print("\n[INFO] Moving the robot to the initial position...")
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 4
    joint_goal[2] = tau / 4
    joint_goal[3] = -tau / 4
    joint_goal[4] = -tau / 4
    joint_goal[5] = tau / 4

    move_group.go(joint_goal, wait=True)
    move_group.stop()

    rospy.sleep(10)

    print("[INFO] Robot moved to starting position:")
    print(move_group.get_current_pose().pose)


def print_pose_info(move_group, pose_description):
    """
    Prints the joint angles and end-effector pose for a given robot configuration.
    """
    print(f"\n[INFO] {pose_description}")
    joint_values = move_group.get_current_joint_values()
    print("Joint Values:", joint_values)

    ee_pose = move_group.get_current_pose().pose
    print("End-effector Pose:", ee_pose)


def draw(move_group):
    """
    Plans and executes a Cartesian path to draw the letter 'C'.
    """
    print("\n[INFO] Preparing to draw the letter 'C'...")
    waypoints = []

    wpose = move_group.get_current_pose().pose
    start_pose = copy.deepcopy(wpose)

    arc_length = 20
    for i in range(arc_length):
        angle = tau * (i / float(arc_length))
        wpose.position.x += cos(angle) * 0.01
        wpose.position.y += sin(angle) * 0.01
        waypoints.append(copy.deepcopy(wpose))

    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)

    move_group.set_pose_target(start_pose)
    move_group.go(wait=True)
    move_group.stop()
    print_pose_info(move_group, "Starting Pose")

    if move_group.execute(plan, wait=True):
        print("[INFO] Drawing 'C' complete.")
    else:
        print("[ERROR] Execution failed.")
    move_group.clear_pose_targets()

    move_group.set_pose_target(waypoints[10])
    move_group.go(wait=True)
    move_group.stop()
    print_pose_info(move_group, "Pose at Waypoint 10")

    move_group.set_pose_target(waypoints[-1])
    move_group.go(wait=True)
    move_group.stop()
    print_pose_info(move_group, "Pose at Final Waypoint")


def main():
    move_group = initialize()
    move_to_start(move_group)
    draw(move_group)


if __name__ == '__main__':
    main()
