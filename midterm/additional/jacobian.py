import numpy as np
from sympy import symbols, cos, sin, pi, Matrix

# Define symbols for joint angles
theta = symbols('theta1:7')  # theta1, theta2, ..., theta6

# UR5e DH parameters for each joint, assumed from your provided data
# Each row: [a, alpha, d, theta]
dh_params = [
    {'a': 0,      'alpha': pi/2, 'd': 0.1625, 'theta': theta[0]},
    {'a': -0.425, 'alpha': 0,    'd': 0,      'theta': theta[1]},
    {'a': -0.3922, 'alpha': 0,    'd': 0,      'theta': theta[2]},
    {'a': 0,      'alpha': pi/2, 'd': 0.1333, 'theta': theta[3]},
    {'a': 0,      'alpha': -pi/2, 'd': 0.0997, 'theta': theta[4]},
    {'a': 0,      'alpha': 0,    'd': 0.0996, 'theta': theta[5]}
]

# Function to calculate the individual transformation matrix for each joint


def dh_transform(a, alpha, d, theta):
    T = Matrix([
        [cos(theta), -sin(theta), 0, a],
        [sin(theta)*cos(alpha), cos(theta) *
         cos(alpha), -sin(alpha), -sin(alpha)*d],
        [sin(theta)*sin(alpha), cos(theta) *
         sin(alpha), cos(alpha), cos(alpha)*d],
        [0, 0, 0, 1]
    ])
    return T


# Compute the overall transformation matrix T0_6
T0_6 = Matrix.eye(4)
for i in range(6):
    T0_6 *= dh_transform(**dh_params[i])

# Extract the position of the end-effector
ee_position = T0_6[:3, 3]

# Compute the Jacobian for the position
J_position = ee_position.jacobian(theta)

# Define joint values for the three key poses
joint_values_start = {theta[0]: -0.0002867425993349215, theta[1]: -1.567529449518605, theta[2]                      : 1.5858121630304716, theta[3]: -1.5694617160770132, theta[4]: -1.5703128205091232, theta[5]: 1.5700482457476985}
joint_values_waypoint_10 = {theta[0]: 0.12688563278320775, theta[1]: -1.5254620648097532, theta[2]                            : 1.5425004423340978, theta[3]: -1.5688737515479536, theta[4]: -1.5691553618664722, theta[5]: 1.69829849768053}
joint_values_final_waypoint = {theta[0]: -9.83060288648474e-05, theta[1]: -1.5676477323701636, theta[2]                               : 1.5854798800156233, theta[3]: -1.5693655542367955, theta[4]: -1.5703003655735035, theta[5]: 1.5709378189398464}

# Function to evaluate the Jacobian matrix at given joint values


def evaluate_jacobian(joint_values):
    J_num = J_position.evalf(subs=joint_values)
    return np.array(J_num).astype(np.float64)


# Evaluate and print the Jacobian matrices for the three key poses
J_start = evaluate_jacobian(joint_values_start)
print("Jacobian at Starting Pose:\n", J_start)

J_waypoint_10 = evaluate_jacobian(joint_values_waypoint_10)
print("\nJacobian at Waypoint 10:\n", J_waypoint_10)

J_final_waypoint = evaluate_jacobian(joint_values_final_waypoint)
print("\nJacobian at Final Waypoint:\n", J_final_waypoint)
