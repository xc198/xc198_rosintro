from sympy import symbols, cos, sin, pi, Matrix
from sympy.matrices import Matrix
import numpy as np

# Define symbols for joint angles and link lengths
theta1, theta2, theta3, theta4, theta5, theta6 = symbols(
    'theta1 theta2 theta3 theta4 theta5 theta6')
a1, a2, a3, d1, d4, d5, d6 = symbols('a1 a2 a3 d1 d4 d5 d6')

# UR5e DH parameters table: theta, d, a, alpha
dh_params = {
    'theta': [theta1, theta2, theta3, theta4, theta5, theta6],
    'd': [0.1625, 0, 0, 0.1333, 0.0997, 0.0996],
    'a': [0, -0.425, -0.3922, 0, 0, 0],
    'alpha': [pi/2, 0, 0, pi/2, -pi/2, 0],
}

# Function to calculate the transformation matrix using DH parameters


def dh_matrix(theta, d, a, alpha):
    return Matrix([
        [cos(theta), -sin(theta)*cos(alpha),
         sin(theta)*sin(alpha), a*cos(theta)],
        [sin(theta),  cos(theta)*cos(alpha), -
         cos(theta)*sin(alpha), a*sin(theta)],
        [0,                    sin(alpha),
         cos(alpha),             d],
        [0,                           0,                     0,             1]
    ])

# Function to compute the overall transformation matrix from base to end-effector


def forward_kinematics(dh_params):
    T = Matrix.eye(4)
    for i in range(6):
        T *= dh_matrix(dh_params['theta'][i], dh_params['d']
                       [i], dh_params['a'][i], dh_params['alpha'][i])
    return T


# Calculate the transformation matrix from base to end-effector
T0_6 = forward_kinematics(dh_params)


# Substitute the actual joint angles into the symbolic matrices for three key poses
# Note: These values are the joint values converted from radians to the symbolic representation
# For actual computation, you need to provide the values in radians
joint_values_start = {theta1: -0.0002867425993349215, theta2: -1.567529449518605, theta3: 1.5858121630304716,
                      theta4: -1.5694617160770132, theta5: -1.5703128205091232, theta6: 1.5700482457476985}
joint_values_waypoint_10 = {theta1: 0.12688563278320775, theta2: -1.5254620648097532,
                            theta3: 1.5425004423340978, theta4: -1.5688737515479536, theta5: -1.5691553618664722, theta6: 1.69829849768053}
joint_values_final_waypoint = {theta1: -9.83060288648474e-05, theta2: -1.5676477323701636,
                               theta3: 1.5854798800156233, theta4: -1.5693655542367955, theta5: -1.5703003655735035, theta6: 1.5709378189398464}

# Forward kinematics for three key poses
FK_start = T0_6.subs(joint_values_start)
FK_waypoint_10 = T0_6.subs(joint_values_waypoint_10)
FK_final_waypoint = T0_6.subs(joint_values_final_waypoint)


# Print the results
print("Forward Kinematics for Starting Pose:\n", FK_start)
print("Forward Kinematics for Waypoint 10:\n", FK_waypoint_10)
print("Forward Kinematics for Final Waypoint:\n", FK_final_waypoint)
