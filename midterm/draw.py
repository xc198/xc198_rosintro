'''
This program uses MoveIt! to move the UR5 robot arm to draw my initials C.C.
'''

from __future__ import print_function
import sys
import copy
import time
from math import pi, tau, cos, sin

import rospy
import moveit_commander
from moveit_commander.conversions import pose_to_list

# Define tau as 2 times pi for convenience
tau = 2.0 * pi


def initialize():
    '''
    Initializes the MoveIt! commander and ROS node.

    Returns:
        move_group (MoveGroupCommander object): The primary interface to the manipulator group of joints.
    '''
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("C_motion_planning", anonymous=True)

    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    print("\n[INFO] Current robot pose:")
    print(move_group.get_current_pose().pose)
    return move_group


def move_to_start(move_group):
    '''
    Moves the robot to the starting position for drawing the letter C.

    Parameters:
        move_group (MoveGroupCommander object): The interface to the manipulator group of joints.
    '''
    print("\n[INFO] Moving the robot to the initial position...")
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0
    joint_goal[1] = -tau / 4
    joint_goal[2] = tau/4
    joint_goal[3] = -tau / 4
    joint_goal[4] = -tau/4
    joint_goal[5] = tau/4

    move_group.go(joint_goal, wait=True)
    move_group.stop()

    rospy.sleep(10)

    print("[INFO] Robot moved to starting position:")
    print(move_group.get_current_pose().pose)

    return joint_goal


def draw(move_group):
    '''
    Plans and executes a Cartesian path to draw the letter 'C'.

    Parameters:
        move_group (MoveGroupCommander object): The interface to the manipulator group of joints.
    '''
    print("\n[INFO] Preparing to draw the letter 'C' in 7 seconds...")
    time.sleep(7)

    waypoints = []

    # Get the current pose to use as a starting point
    wpose = move_group.get_current_pose().pose

    # Generate waypoints for a semi-circular path to draw 'C'
    arc_length = 20  # Number of waypoints in the arc
    print("\n[INFO] Generating waypoints for the letter 'C':")
    for i in range(arc_length):
        # Calculate angle for each waypoint
        angle = tau * (i / float(arc_length))
        wpose.position.x += cos(angle) * 0.01  # X-coordinate
        wpose.position.y += sin(angle) * 0.01  # Y-coordinate
        print(
            f"  Waypoint {i + 1}: x = {wpose.position.x:.4f}, y = {wpose.position.y:.4f}")
        waypoints.append(copy.deepcopy(wpose))

    # Compute Cartesian path
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    print(
        f"\n[INFO] Path planning complete. Path coverage: {fraction * 100:.2f}% of waypoints.")

    # Execute the planned path
    if move_group.execute(plan, wait=True):
        print("[INFO] Drawing 'C' complete.")
    else:
        print("[ERROR] Execution failed.")
    move_group.clear_pose_targets()


def main():
    '''
    Main function to execute the program.
    '''
    move_group = initialize()
    move_to_start(move_group)
    draw(move_group)
    draw(move_group)


# Run the main function
main()
